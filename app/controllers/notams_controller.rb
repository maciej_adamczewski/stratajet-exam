class NotamsController < ApplicationController
  before_action :set_notam, only: [:show, :edit, :update, :destroy]

  # GET /notams
  # GET /notams.json
  def index
    @notams = Notam.all
  end

  # GET /notams/1
  # GET /notams/1.json
  def show
  end

  # GET /notams/new
  def new
    @notam = Notam.new
  end

  # GET /notams/1/edit
  def edit
  end
  # METHOD  for collecting day and putting them in right order 
  def create_time_table(time_table, day, r)
    flat_r = r.flatten
    case day
    when "MON" 
      time_table.day1 = flat_r.join(',') 
    when "TUE"
      time_table.day2 = flat_r.join(',')
    when "WED"
      time_table.day3 = flat_r.join(',')
    when "THU"
      time_table.day4 = flat_r.join(',')
    when "FRI"
      time_table.day5 = flat_r.join(',')
    when "SAT"
      time_table.day6 = flat_r.join(',')
    when "SUN"
      time_table.day7 = flat_r.join(',')
    end
  end

  # POST /notams
  # POST /notams.json
  def create
    my_notam = notam_params[:notam_value]
    # Define Days
    days = [ "MON" , "TUE" , "WED" , "THU" , "FRI" , "SAT" , "SUN" ]
    # Get All NOTAMS
    all_notams = my_notam.scan(/A[)]([\s\S]*?)CREATED/)
    all_notams.each do |notam|
      # Get ICAO code form notam 
      icao_code = notam[0].scan(/\s([\s\S]*?)\sB[)]/)
      icao_code_flatten = icao_code.flatten
      time_table = TimeTable.new(icao_code: icao_code_flatten.join(','));
      # Clear all_notams from formatings
      datas = notam[0].gsub(/\r\n|\r|\n/," ")
      # Find Hours in ranges
      days.each_with_index do |day, index|
        days.each_with_index do |day2, index2|
          range2 = datas.scan(/#{day}-#{day2}\s(\d{4}-\d{4})/)
          if range2.empty? == false
            range2.each do |r|
              (index..index2).each do |d|
                create_time_table(time_table, days[d], r)
              end
            end
          end
          range3 = datas.scan(/#{day}-#{day2}[(\s\S)](\d{4}-\d{4},\s\d{4}-\d{4})/)
          if range3.empty? == false
            range3.each do |r|
              (index..index2).each do |d|
                create_time_table(time_table, days[d], r)
              end
            end
          end
        end
        # Find daily working hours
        i1 = datas.scan(/\s#{day}[(\s\S){0,2}](\d{4}-\d{4})\s/)
        i2 = datas.scan(/\s#{day}[(\s\S){0,2}](\d{4}-\d{4},\s\d{4}-\d{4})/)
        i3 = datas.scan(/\s#{day}[(\s\S){0,2}](\d{4}-\d{4},\s\d{4}-\d{4},\s\d{4}-\d{4})/)
        i4 = datas.scan(/\s#{day}[(\s\S){0,2}](\d{4}-\d{4},\s\d{4}-\d{4},\s\d{4}-\d{4},\s\d{4}-\d{4})/)
        ic = datas.scan(/\s#{day}[(\s\S){0,2}](CLOSED)/)
        ic2 = datas.scan(/\s#{day}[(\s\S){0,2}](CLSD)/)
        create_time_table(time_table, day, i1) if i1.present? || i1.nil?
        create_time_table(time_table, day, i2) if i2.present? || i2.nil? 
        create_time_table(time_table, day, i3) if i3.present? || i3.nil?
        create_time_table(time_table, day, i4) if i4.present? || i4.nil?
        create_time_table(time_table, day, [[ic]]) if ic.present? || ic.nil?
        create_time_table(time_table, day, [["CLOSED"]]) if ic2.present? || ic2.nil?
      end
      # write data
      time_table.save!
    end
    redirect_to time_tables_path
  end

  # PATCH/PUT /notams/1
  # PATCH/PUT /notams/1.json
  def update
    respond_to do |format|
      if @notam.update(notam_params)
        format.html { redirect_to @notam, notice: 'Notam was successfully updated.' }
        format.json { render :show, status: :ok, location: @notam }
      else
        format.html { render :edit }
        format.json { render json: @notam.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notams/1
  # DELETE /notams/1.json
  def destroy
    @notam.destroy
    respond_to do |format|
      format.html { redirect_to notams_url, notice: 'Notam was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notam
      @notam = Notam.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notam_params
      params.require(:notam).permit(:notam_value)
    end
end
