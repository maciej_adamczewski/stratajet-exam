json.array!(@notams) do |notam|
  json.extract! notam, :id, :notam_value
  json.url notam_url(notam, format: :json)
end
