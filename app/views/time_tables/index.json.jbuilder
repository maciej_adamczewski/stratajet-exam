json.array!(@time_tables) do |time_table|
  json.extract! time_table, :id, :icao_code, :day1, :day2, :day3, :day4, :day5, :day6, :day7
  json.url time_table_url(time_table, format: :json)
end
