require 'test_helper'

class NotamsControllerTest < ActionController::TestCase
  setup do
    @notam = notams(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:notams)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create notam" do
    assert_difference('Notam.count') do
      post :create, notam: { notam_value: @notam.notam_value }
    end

    assert_redirected_to notam_path(assigns(:notam))
  end

  test "should show notam" do
    get :show, id: @notam
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @notam
    assert_response :success
  end

  test "should update notam" do
    patch :update, id: @notam, notam: { notam_value: @notam.notam_value }
    assert_redirected_to notam_path(assigns(:notam))
  end

  test "should destroy notam" do
    assert_difference('Notam.count', -1) do
      delete :destroy, id: @notam
    end

    assert_redirected_to notams_path
  end
end
