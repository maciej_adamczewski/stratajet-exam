class CreateNotams < ActiveRecord::Migration
  def change
    create_table :notams do |t|
      t.text :notam_value

      t.timestamps
    end
  end
end
